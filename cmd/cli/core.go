package main

import (
	"gitlab.com/Spouk/blog/pkg/utils"
	"io"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

const prefix = "[webserver] "

type (
	Core struct {
		log  *log.Logger
		cfg  *Config
		sigs chan os.Signal
		sw   sync.WaitGroup
	}
)

func NewCore(configfile, logfile string) (*Core, error) {
	//create new core instance
	core := &Core{
		log:  log.New(os.Stdout, prefix, log.LstdFlags),
		sigs: make(chan os.Signal, 1),
		sw:   sync.WaitGroup{},
	}

	//open/create log file
	fh, err := utils.OpenLogFile(logfile)
	if err != nil {
		return core, err
	}
	//set multiout logging
	core.log.SetOutput(io.MultiWriter(fh, os.Stdout))

	//open/read config file
	var cfg Config
	err = utils.Readyamlfile(configfile, &cfg)
	if err != nil {
		return core, err
	}
	core.cfg = &cfg

	//set list sig* for catching
	signal.Notify(core.sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP, syscall.SIGKILL, syscall.SIGSTOP)

	//run sig* catcher
	go core.catcherSigKiller()

	//return result
	return core, nil
}

func (core *Core) catcherSigKiller() {
	//cathing interrupt, make need actions
	sig := <-core.sigs
	core.log.Printf("[warning] пойман сигнал прерывания работы сервера `%v`,  что не есть хорошо!, подождите немного пока я корректно завершу работу, ок? я так и думал... ", sig)

	//save sessions from memory to database
	//2 path: release middleware after middleware session extract session object
	//adding information/check user authorization and more actions and goto next context layer

	//working need for save open files/dump configs and more  actions
	time.Sleep(time.Second * 2)

	//exit
	os.Exit(1)
}
func (core *Core) Run() {
	core.log.Printf("wwwserver starting...")
	core.sw.Add(1)
	core.sw.Wait()
	core.log.Printf("wwwserver end working, cya  ;)")
	os.Exit(1)
}
