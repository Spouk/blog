package main

import (
	"time"
)

type Config struct {
	LogFile              string        `yaml:"logfile"`
	ConfigPath           string        `yaml:"configpath"`
	ConfigFile           string        `yaml:"configfile"`
	ConfigDumpPath       string        `yaml:"config_dump_path"`
	ConfigDumpFile       string        `yaml:"config_dump_file"`
	CoreDebug            bool          `yaml:"coredebug"`
	Hostname             string        `yaml:"hostname"`
	Templatepath         string        `yaml:"templatepath"`
	Templatedebug        bool          `yaml:"templatedebug"`
	Teamplatedebugfatal  bool          `yaml:"teamplatedebugfatal"`
	Readtimeout          int           `yaml:"readtimeout"`
	Writetimeout         int           `yaml:"writetimeout"`
	Adresshttp           string        `yaml:"adresshttp"`
	Certfile             string        `yaml:"certfile"`
	Keyfile              string        `yaml:"keyfile"`
	Roles                []string      `yaml:"roles"`
	RolesAdmin           []string      `yaml:"rolesadmin"`
	DBTypeDB             string        `yaml:"dbtypedb"`
	DBHost               string        `yaml:"dbhost"`
	DBPort               string        `yaml:"dbport"`
	DBUser               string        `yaml:"dbuser"`
	DBPassword           string        `yaml:"dbpassword"`
	DBDatabase           string        `yaml:"dbdatabase"`
	DBSSLMode            bool          `yaml:"dbsslmode"`
	DBSetMaxIdleConns    int           `yaml:"dbsetmaxidleconns"`
	DBSetMaxOpenConns    int           `yaml:"dbsetmaxopenconns"`
	DBSetConnMaxLifetime int           `yaml:"dbsetconnmaxlifetime"`
	PaginateCountOnPage  int           `yaml:"paginatecountonpage"`
	PaginateCountLinks   int           `yaml:"paginatecountlinks"`
	PaginateSortType     []string      `yaml:"paginatesorttype"`
	PaginateDebug        bool          `yaml:"paginatedebug"`
	UploadPath           string        `yaml:"uploadpath"`
	SitemapPath          string        `yaml:"sitemappath"`
	SitemapHost          string        `yaml:"sitemaphost"`
	HostFullPathHTTP     string        `yaml:"hostfullpathhttp"`
	HostFullPathHTTPS    string        `yaml:"hostfullpathhttps"`
	SeoTitle             string        `yaml:"seotitle"`
	SeoDesc              string        `yaml:"seodesc"`
	SeoKeys              string        `yaml:"seokeys"`
	SeoRobot             string        `yaml:"seorobot"`
	LaterPostTimePeriod  int           `yaml:"laterposttimeperiod"`
	MailTo               string        `yaml:"mailto"`
	MailFrom             string        `yaml:"mailfrom"`
	MailHost             string        `yaml:"mailhost"`
	MailPort             int           `yaml:"mailport"`
	MailUsername         string        `yaml:"mailusername"`
	MailPassword         string        `yaml:"mailpassword"`
	CSRFTimeActive       int           `yaml:"csrftimeactive"`
	CSRFSalt             string        `yaml:"csrfsalt"`
	CookieName           string        `yaml:"cookiename"`
	CookiePath           string        `yaml:"cookiepath"`
	CookieDomain         string        `yaml:"cookiedomain"`
	CookieExpired        time.Duration `yaml:"cookieexpired"`
	CookieSalt           string        `yaml:"cookiesalt"`
	RoleDefaultUser      string        `yaml:"roledefaultuser"`
	SessionTime          time.Duration `yaml:"sessiontime"`
	SessionTimeExpired   time.Duration `yaml:"sessiontimeexpired"`
	SessionTimeSave      time.Duration `yaml:"sessiontimesave"`
	SessionPathSave      string        `yaml:"pathsavesession"`
	TimerTime            time.Duration `yaml:"timertime"`
	SleepTimeCatcher     time.Duration `yaml:"sleeptimecatcher"`
	DeferPostSleepTime   time.Duration `yaml:"deferpostsleeptime"`
	DeferPostTime        time.Duration `yaml:"deferposttime"`
	ContactReview        []string      `yaml:"contactreview"`
	FlashSalt            string        `yaml:"flashsatl"`
	DefaultAdminEmail    string        `yaml:"defaultadminemail"`
	DefaultAdminPassword string        `yaml:"defaultadminpassword"`
	DefaultAdminRole     string        `yaml:"defaultadminrole"`
	DefaultAdminLogin    string        `yaml:"defaultadminlogin"`
}
