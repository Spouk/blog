package main

import (
	"flag"
	"fmt"
	"os"
	"strings"
)

var version = "0.1.0"

func main() {
	var (
		cf = flag.String("config", "", "config file with full path")
		lf = flag.String("log", "", "logfile with full path")
	)
	flag.Parse()
	if *cf == "" || *lf == "" {
		var line []string
		for x := 0; x < 45; x++ {
			line = append(line, "=")
		}
		fmt.Printf("%v\n wwwserver [x] aleksey.martynenko//  cyberspouk@gmail.com ||"+
			"spouk@spouk.ru ,  version %v\n%v\n\n", strings.Join(line, " "), version, strings.Join(line, " "))
		flag.PrintDefaults()
		os.Exit(1)
	}
	core, err := NewCore(*cf, *lf)
	if err != nil {
		core.log.Printf("FATALERROR: %v", err)
		os.Exit(1)
	}
	core.Run()
}
