//-------------------------------------------------------------------------------
// пакет реализующий функционал по работе с базой данных
//-------------------------------------------------------------------------------

package dbs

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
	"os"
)

const (
	prefix   = "[dbs] "
	errpanic = iota
	errwarning
	errinfo
)

type (
	DBSCore struct {
		log *log.Logger
		cfg DBSCoreConfig
		db  *gorm.DB
	}
	DBSCoreConfig struct {
		ExternLog *log.Logger
		Cinfo     ConnectInfo
	}
	ConnectInfo struct {
		Host     string
		Port     string
		User     string
		Password string
		Dbname   string
	}
)

// NewDBSCore -------------------------------------------------------------------------------
// DBSCore
//-------------------------------------------------------------------------------
func NewDBSCore(cfg DBSCoreConfig) *DBSCore {
	n := &DBSCore{
		cfg: cfg,
	}
	if cfg.ExternLog != nil {
		n.log = cfg.ExternLog
	} else {
		n.log = log.New(os.Stdout, prefix, log.LstdFlags)
	}
	return n
}

// Disconnect close active connection to database
func (d *DBSCore) Disconnect() {
	dbs, err := d.db.DB()
	d.checkError(errwarning, err)
	d.checkError(errwarning, dbs.Close())
}

// Connect establishing connection to database
func (d *DBSCore) Connect() {
	db, err := gorm.Open(postgres.Open(d.cfg.Cinfo.ToDSN()), &gorm.Config{})
	d.checkError(errpanic, err)

	// check db
	d.checkError(errpanic, err)

	d.log.Printf("success connecting database %v", d.cfg.Cinfo.Host)
	d.db = db
}
func (d DBSCore) checkError(errlvl int, err error) {
	switch errlvl {
	case errpanic:
		d.log.Fatal(err)
	case errinfo:
		d.log.Printf("[errorinfo] %v", err)
	case errwarning:
		d.log.Printf("[errorwarning] %v", err)
	default:
		d.log.Printf("[errorwrongtype] %v -> %v", errlvl, err)
	}
}

// CreateModels create models
func (d DBSCore) CreateModels() {
	for k, v := range listModels {
		d.log.Printf("creating %v", k)
		err := d.db.AutoMigrate(&v)
		if err != nil {
			d.log.Printf("[%s]error creating %v", k, err)
		} else {
			d.log.Printf("[%s]success creating ")
		}
	}
}

// ToDSN -------------------------------------------------------------------------------
// connectinfo
//-------------------------------------------------------------------------------
func (c ConnectInfo) ToDSN() string {
	return fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", c.Host, c.Port, c.User, c.Password, c.Dbname)
}
