// Package dbs -------------------------------------------------------------------------------
// table gorm models
//-------------------------------------------------------------------------------
package dbs

import (
	"gorm.io/gorm"
	"time"
)

var (
	listModels = map[string]interface{}{
		"Sessions":  Sessions{},
		"Users":     Users{},
		"Categorys": Categorys{},
		"Articles":  Articles{},
		"Posts":     Posts{},
		"Roles":     Roles{},
		"Comments":  Comments{},
		"Files":     Files{},
	}
)

type (
	Sessions struct {
		gorm.Model
		Userid      uint   // fk : users.id
		Cook        string `sql:"type:varchar(190); unique"` //cook = ID
		Active      bool
		LastConnect time.Time
	}
	Users struct {
		gorm.Model
		Rolesid      uint    //fk: roles.id
		Login        string  `sql:"type:varchar(200); unique"`
		Email        string  `sql:"type:varchar(190); unique"`
		Passwordhash string  `sql:"type:varchar(500)"`
		Cook         string  `sql:"type:varchar(200)"`
		Avatars      [3]uint `sql:"type:integer[3]"`
		Connected    bool
		Logged       bool
	}
	Roles struct {
		gorm.Model
		Name string `sql:"type:text, unique"`
	}
	Posts struct {
		gorm.Model
		Authorsid   uint   //fk: users id
		Categorysid uint   //fk: category id
		Articlesid  uint   //fk: articles id
		Commentsids []uint `sql:"type:integer[]"` //(fk*): [comments.id]uint
		Active      bool
		PreBody     string `sql:"type:text"`
		MetaKeys    string `sql:"type:text"`
		MetaDesc    string `sql:"type:text"`
		MetaRobot   string `sql:"type:text"`
		MetaTitle   string `sql:"type:text"`
	}
	Categorys struct {
		gorm.Model
		Name  string `sql:"type:text, unique"`
		Alias string `sql:"type:text, unique"`
	}
	Articles struct {
		gorm.Model
		Name      string `sql:"type:text, unique"`
		Alias     string `sql:"type:text, unique"`
		Data      string `sql:"type:text"`
		Imagesids []uint `sql:"type:integer[]"`
	}
	Comments struct {
		gorm.Model
		Authorid      uint   //fk: users.id
		Replcommentid uint   //fk: comments.id = if reply comment
		Postsid       uint   //fk: posts.id
		Data          string `sql:"type:text"`
	}
	Files struct {
		gorm.Model
		Name     string
		Ext      string
		Withpath string
	}
)
